# ASSIGNMENT 10

## Student.java

```java
package org.antwalk;

public class Student {
    
    private String name;
    
    private float engMarks; 
    
    private float hindiMarks;
    
    private float mathsMarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getEngMarks() {
        return engMarks;
    }

    public void setEngMarks(float engMarks) {
        this.engMarks = engMarks;
    }

    public float getHindiMarks() {
        return hindiMarks;
    }

    public void setHindiMarks(float hindiMarks) {
        this.hindiMarks = hindiMarks;
    }

    public float getMathsMarks() {
        return mathsMarks;
    }

    public void setMathsMarks(float mathsMarks) {
        this.mathsMarks = mathsMarks;
    }

    public void calculateAvgMark(){
        System.out.println("The average marks are "+(engMarks+hindiMarks+mathsMarks)/3);
    }

    public void showResult(){
        System.out.println("------------Statement of marks---------");
        System.out.println("Name: "+name);
        System.out.println("=======================================");
        System.out.println("English\t||\tHindi\t||\tMaths");
        System.out.println(engMarks+"\t||\t"+hindiMarks+"\t||\t"+mathsMarks);
    }

}
```

## beans.xml

```xml
<?xml version = "1.0" encoding = "UTF-8"?>

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
   http://www.springframework.org/schema/beans/spring-beans-3.0.xsd">


	<bean id="beforeMethodBean" class="org.antwalk.BeforeMethod" />
	
	<bean id="afterMethodBean" class="org.antwalk.AfterMethod" />

	<bean id="student" class="org.antwalk.Student">
		<property name="name" value="Name1"></property>
		<property name="engMarks" value="89"></property>
		<property name="hindiMarks" value="86"></property>
		<property name="mathsMarks" value="83"></property>
	</bean>

	<bean id="studentWithBeforeAfterAdvice" 
		class="org.springframework.aop.framework.ProxyFactoryBean">
		<property name="target" ref="student"></property>
		<property name="interceptorNames">
			<list>
				<value>afterMethodBean</value>
				<value>beforeMethodBean</value>
			</list>
		</property>
		
	</bean>
	
</beans>
```

## Test.java

```java
package org.antwalk;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		Student student = context.getBean("studentWithBeforeAfterAdvice", Student.class);
		
		student.calculateAvgMark();

		System.out.println();
		System.out.println();
		System.out.println();
		
		student.showResult();
	}

}
```

## Output

```Terminal
C:\Users\ss\Desktop\AOPApplication> c: && cd c:\Users\ss\Desktop\AOPApplication && cmd /C ""C:\Program Files\Eclipse Adoptium\jdk-8.0.322.6-hotspot\bin\java.exe" -cp C:\Users\ss\AppData\Local\Temp\cp_p477th1d7tectd2wev4mf9xv.jar org.antwalk.Test "


======================Before method executed
The average marks are 86.0
====================after method executed



======================Before method executed
------------Statement of marks---------
Name: Name1
=======================================
English ||      Hindi   ||      Maths
89.0    ||      86.0    ||      83.0
====================after method executed

C:\Users\ss\Desktop\AOPApplication>
```