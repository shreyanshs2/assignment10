package org.antwalk;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		Student student = context.getBean("studentWithBeforeAfterAdvice", Student.class);
		
		System.out.println();
		
		student.calculateAvgMark();

		System.out.println();
		System.out.println();

		student.showResult();
	}

}
