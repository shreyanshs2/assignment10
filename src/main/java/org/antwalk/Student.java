package org.antwalk;

public class Student {
    
    private String name;
    
    private float engMarks; 
    
    private float hindiMarks;
    
    private float mathsMarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getEngMarks() {
        return engMarks;
    }

    public void setEngMarks(float engMarks) {
        this.engMarks = engMarks;
    }

    public float getHindiMarks() {
        return hindiMarks;
    }

    public void setHindiMarks(float hindiMarks) {
        this.hindiMarks = hindiMarks;
    }

    public float getMathsMarks() {
        return mathsMarks;
    }

    public void setMathsMarks(float mathsMarks) {
        this.mathsMarks = mathsMarks;
    }

    public void calculateAvgMark(){
        System.out.println("The average marks are "+(engMarks+hindiMarks+mathsMarks)/3);
    }

    public void showResult(){
        System.out.println("------------Statement of marks---------");
        System.out.println("Name: "+name);
        System.out.println("=======================================");
        System.out.println("English\t||\tHindi\t||\tMaths");
        System.out.println(engMarks+"\t||\t"+hindiMarks+"\t||\t"+mathsMarks);
    }

}
